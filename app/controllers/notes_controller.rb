class NotesController < ApplicationController
  def create
    Note.create(note_params.merge(user_id: current_user.id))
  end
  def delete
    Note.find(params[:id]).destroy(note_params)
  end
  def update
    Note.find(params[:id]).update(note_params)
  end
  def notes_params
    params.require(:note).permit(:status_id, :body)
  end
end
