class StatusesController < ApplicationController
  def new
    @status = Status.new
  end
  def create
    Status.create(status_params.merge(board_id: current_user.id))
    redirect_to :root
  end
  def delete
    Status.find(params[:id]).destroy(status_params)
  end
  def update
    Status.find(params[:id]).update(status_params)
  end
  def show
    @status = Status.find(params[:id])
  end
  def status_params
    params.require(:status).permit(:board_id, :name)
  end
end

