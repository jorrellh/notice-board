class BoardsController < ApplicationController
  def index
    @boards = Board.all.order(created_at: :desc)
  end
  def create
    Board.create(board_params.merge(user_id: current_user.id))
    redirect_to boards_url
  end
  def new
    @board = Board.new
  end
  def show
    @board = Board.find(params[:id])
  end
  def board_params
    params.require(:board).permit(:title)
  end

end
