class Add < ActiveRecord::Migration[5.1]
  def change
    add_column :boards, :title, :string
    add_reference :boards, :user_id
    add_column :statuses, :name, :string
    add_reference :statuses, :board_id
    add_column :notes, :content, :string
    add_reference :notes, :status_id
  end
end
