class ChangeUser < ActiveRecord::Migration[5.1]
  def change
    rename_column :boards, :user_id_id, :user_id
    rename_column :statuses, :board_id_id, :board_id
    rename_column :notes, :status_id_id, :status_id
  end
end